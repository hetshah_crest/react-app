import React, { Component } from 'react';

class Counter extends Component {

    styles = {
        fontSize: 10,
        fontWeight: 'bold'
    }

    render() {

        return (

            <div>
                <span style={this.styles} className={this.getBadgeClasses()}>{this.formatCounter()}</span>
                <button onClick={() => this.props.onIncrement(this.props.counter)} style={{ fontSize: 20 }} className='btn btn-secondary btn-sm'>Increment</button>
                {/* <ul>
                    {this.renderTags()}
                </ul> */}
                <button onClick={() => this.props.onDelete(this.props.counter.id)} className="btn btn-danger btn-sm m-2">Delete</button>
            </div>
        );
    }
    getBadgeClasses() {
        let classes = 'badge m2 badge-';
        classes += (this.props.counter.value === 0) ? 'warning' : 'primary';
        return classes;
    }

    formatCounter() {
        const { value } = this.props.counter //count property of state destructed in a variable
        return value === 0 ? 'Zero' : value //if count=0 return Zero else count
    }


}

export default Counter;