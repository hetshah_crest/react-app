import './App.css';
import NavBar from './components/navbar';
import React, { Component } from 'react';
import { useState } from 'react';
import Counters from './components/counters';


function App() {
  const [counters, setCount] = useState([
    { id: 1, value: 0 },
    { id: 2, value: 1 },
    { id: 3, value: 2 },
    { id: 4, value: 0 }
  ])

  return (
    <React.Fragment>
      <NavBar />
      <main className='container'>
        <Counters
          counters={counters}
          onReset={() => {
            const dummyCounter = [...counters];
            dummyCounter.map(count => setCount(count.value = 0));
            setCount(dummyCounter);

          }}
          onIncrement={(counter) => {
            const dummyCounter = [...counters];
            const index = counters.indexOf(counter)
            dummyCounter[index] = { ...counter }
            dummyCounter[index].value++
            setCount(dummyCounter);
          }}
          onDelete={(counterId) => {
            const dummyCounter = counters.filter(c => c.id !== counterId);
            setCount(dummyCounter);
          }}
        />
      </main>
    </React.Fragment>
  );
}

export default App;
